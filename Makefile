REBAR = $(shell pwd)/rebar3
ELVIS = $(shell pwd)/elvis
APP=tachyon

.PHONY: rel stagedevrel package all tree

all: version_header compile

include fifo.mk

version_header:
	@./version_header.sh

clean:
	$(REBAR) clean
	$(MAKE) -C rel/pkg clean

long-test:
	$(REBAR) as eqc,long eunit
